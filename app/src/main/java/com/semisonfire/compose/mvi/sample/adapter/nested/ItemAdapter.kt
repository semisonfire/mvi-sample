package com.semisonfire.compose.mvi.sample.adapter.nested

import android.view.ViewGroup
import androidx.recyclerview.widget.AdapterListUpdateCallback
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.semisonfire.compose.mvi.sample.adapter.Item

class ItemAdapter : RecyclerView.Adapter<ItemViewHolder<out Item>>() {

    private val items = mutableListOf<Item>()

    private val factory = ItemFactory()
    private val adapterListUpdateCallback = AdapterListUpdateCallback(this)

    fun submitList(items: List<Item>) {
        val diff = DiffUtil.calculateDiff(ItemDiffCallback(this.items, items))

        this.items.clear()
        this.items.addAll(items)

        diff.dispatchUpdatesTo(adapterListUpdateCallback)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder<out Item> {
        return factory.create(parent, viewType)
            ?: throw IllegalArgumentException("Can not create view holder for viewType: $viewType")
    }

    override fun onBindViewHolder(holder: ItemViewHolder<out Item>, position: Int) {
        holder as ItemViewHolder<in Item>
        holder.bind(items[position])
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun getItemViewType(position: Int): Int {
        return items[position].getItemType()
    }

    override fun onViewAttachedToWindow(holder: ItemViewHolder<out Item>) {
        super.onViewAttachedToWindow(holder)
        holder.attach()
    }

    override fun onViewDetachedFromWindow(holder: ItemViewHolder<out Item>) {
        super.onViewDetachedFromWindow(holder)
        holder.detach()
    }

    override fun onViewRecycled(holder: ItemViewHolder<out Item>) {
        super.onViewRecycled(holder)
        holder.recycle()
    }
}