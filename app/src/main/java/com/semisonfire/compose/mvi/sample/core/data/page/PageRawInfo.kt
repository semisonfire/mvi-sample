package com.semisonfire.compose.mvi.sample.core.data.page

data class PageRawInfo(
    val current: String,
    val next: String?,
    val data: String
)