package com.semisonfire.compose.mvi.sample.navigation.di

import javax.inject.Scope

@Scope
@Retention
annotation class NavigationScope
