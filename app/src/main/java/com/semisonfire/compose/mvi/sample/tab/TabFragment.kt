package com.semisonfire.compose.mvi.sample.tab

import android.content.Context
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import com.semisonfire.compose.mvi.sample.R
import com.semisonfire.compose.mvi.sample.character.CharacterFragment
import com.semisonfire.compose.mvi.sample.databinding.FragmentTabBinding
import com.semisonfire.compose.mvi.sample.di.provider.ComponentProvider
import com.semisonfire.compose.mvi.sample.di.provider.provideComponent
import com.semisonfire.compose.mvi.sample.navigation.Navigator
import com.semisonfire.compose.mvi.sample.tab.di.DaggerTabComponent
import com.semisonfire.compose.mvi.sample.tab.di.TabComponent
import com.semisonfire.compose.mvi.sample.tab.di.annotation.TabNavigator
import javax.inject.Inject

class TabFragment : Fragment(R.layout.fragment_tab), ComponentProvider<TabComponent> {

    companion object {
        fun newInstance(): Fragment {
            return TabFragment()
        }
    }

    @Inject
    @TabNavigator
    lateinit var navigator: Navigator

    private var componet: TabComponent? = null

    private var _viewBinding: FragmentTabBinding? = null
    private val viewBinding
        get() = _viewBinding!!

    override fun onAttach(context: Context) {
        componet = DaggerTabComponent
            .factory()
            .create(
                fragmentManager = childFragmentManager,
                applicationComponent = context.provideComponent(),
                mainComponent = context.provideComponent()
            )
            .also {
                it.inject(this)
            }

        super.onAttach(context)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (savedInstanceState == null) {
            navigator.replace(R.id.tab_container, CharacterFragment.newInstance())
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        _viewBinding = FragmentTabBinding.bind(view)

        viewBinding.tabNavigation.apply {

            setOnItemSelectedListener {
                when (it.itemId) {
                    R.id.menu_character -> {
                        navigator.replace(R.id.tab_container, CharacterFragment.newInstance())
                    }
                    R.id.menu_episode -> {

                    }
                    R.id.menu_location -> {

                    }
                }
                true
            }
        }
    }

    override fun component(): TabComponent? {
        return componet
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _viewBinding = null
    }
}