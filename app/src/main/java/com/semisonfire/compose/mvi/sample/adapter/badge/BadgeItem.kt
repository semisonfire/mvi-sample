package com.semisonfire.compose.mvi.sample.adapter.badge

import com.semisonfire.compose.mvi.sample.R
import com.semisonfire.compose.mvi.sample.adapter.Item

data class BadgeItem(
    val text: String,
    val color: Int? = null
) : Item {

    override fun getItemType(): Int {
        return R.layout.badge_view
    }

    override fun areItemsSame(item: Item): Boolean {
        return this.getItemType() == item.getItemType()
    }

    override fun areContentSame(item: Item): Boolean {
        return this == item
    }
}