package com.semisonfire.compose.mvi.sample.network

import okhttp3.Interceptor
import okhttp3.Response
import javax.inject.Inject

class NetworkInterceptor @Inject constructor() : Interceptor {

    companion object {
        private const val ACCEPT_KEY = "Accept"
        private const val ACCEPT_VALUE = "application/json"
    }

    override fun intercept(chain: Interceptor.Chain): Response {
        return chain.proceed(
            chain.request()
                .newBuilder()
                .header(ACCEPT_KEY, ACCEPT_VALUE)
                .build()
        )
    }

}
