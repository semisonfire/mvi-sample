package com.semisonfire.compose.mvi.sample.character.di

import com.semisonfire.compose.mvi.sample.character.CharacterFragment
import com.semisonfire.compose.mvi.sample.di.ApplicationComponent
import com.semisonfire.compose.mvi.sample.tab.di.TabComponent
import dagger.Component
import javax.inject.Scope

@Scope
@Retention
annotation class CharacterScope

@CharacterScope
@Component(
    dependencies = [
        ApplicationComponent::class,
        TabComponent::class
    ]
)
interface CharacterComponent {
    fun inject(screen: CharacterFragment)

    @Component.Factory
    interface Factory {

        fun create(
            applicationComponent: ApplicationComponent,
            tabComponent: TabComponent
        ): CharacterComponent
    }
}