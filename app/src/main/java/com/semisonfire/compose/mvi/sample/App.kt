package com.semisonfire.compose.mvi.sample

import android.app.Application
import com.semisonfire.compose.mvi.sample.di.ApplicationComponent
import com.semisonfire.compose.mvi.sample.di.DaggerApplicationComponent
import com.semisonfire.compose.mvi.sample.di.provider.ComponentProvider

class App : Application(), ComponentProvider<ApplicationComponent> {

    lateinit var component: ApplicationComponent

    override fun onCreate() {
        component = DaggerApplicationComponent.factory().create(this)
        super.onCreate()
    }

    override fun component(): ApplicationComponent {
        return component
    }
}