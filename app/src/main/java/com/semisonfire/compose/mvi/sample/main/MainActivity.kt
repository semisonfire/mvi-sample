package com.semisonfire.compose.mvi.sample.main

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.semisonfire.compose.mvi.sample.R
import com.semisonfire.compose.mvi.sample.di.provider.ComponentProvider
import com.semisonfire.compose.mvi.sample.di.provider.provideComponent
import com.semisonfire.compose.mvi.sample.main.di.DaggerMainComponent
import com.semisonfire.compose.mvi.sample.main.di.MainComponent
import com.semisonfire.compose.mvi.sample.main.di.MainComponentApi
import com.semisonfire.compose.mvi.sample.navigation.Navigator
import com.semisonfire.compose.mvi.sample.tab.TabFragment
import javax.inject.Inject

class MainActivity : AppCompatActivity(R.layout.activity_main),
    ComponentProvider<MainComponentApi> {

    @Inject
    lateinit var navigator: Navigator

    private var component: MainComponent? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        component = DaggerMainComponent
            .factory()
            .create(
                supportFragmentManager,
                provideComponent()
            )
            .also {
                it.inject(this)
            }


        super.onCreate(savedInstanceState)

        if (savedInstanceState == null) {

            navigator.open(R.id.screen_fragment_container, TabFragment.newInstance())
        }
    }

    override fun component(): MainComponentApi? {
        return component
    }
}