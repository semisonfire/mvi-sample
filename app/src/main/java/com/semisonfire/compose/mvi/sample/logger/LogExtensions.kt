package com.semisonfire.compose.mvi.sample.logger

import com.semisonfire.compose.mvi.sample.BuildConfig

fun Any.debugLog(message: String) {
    Log.d(javaClass, message)
}

fun Any.infoLog(message: String) {
    Log.i(javaClass, message)
}

fun Any.errorLog(message: String) {
    Log.e(javaClass, message)
}

fun Throwable.printThrowable() {
    Log.log("ERROR", this.message ?: "$javaClass\n", ERROR)

    if (BuildConfig.DEBUG) {
        this.printStackTrace()
    }
}