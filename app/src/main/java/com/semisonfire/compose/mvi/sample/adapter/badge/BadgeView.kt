package com.semisonfire.compose.mvi.sample.adapter.badge

import android.content.Context
import android.content.res.ColorStateList
import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.util.AttributeSet
import android.view.Gravity
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.annotation.ColorInt
import androidx.annotation.DrawableRes
import androidx.core.view.ViewCompat
import androidx.core.view.isVisible
import com.semisonfire.compose.mvi.sample.R
import com.semisonfire.compose.mvi.sample.utils.dip
import com.semisonfire.compose.mvi.sample.utils.setTextOrGone
import com.semisonfire.compose.mvi.sample.utils.show

class BadgeView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : LinearLayout(context, attrs, defStyleAttr) {

    private val iconIv by lazy { findViewById<ImageView>(R.id.iconIv) }
    private val textTv by lazy { findViewById<TextView>(R.id.textTv) }

    init {
        orientation = HORIZONTAL
        gravity = Gravity.CENTER

        setPadding(context.dip(8), 0, context.dip(8), 0)
        minimumHeight = context.dip(24)

        setBackgroundResource(R.drawable.bg_badge_view)
        View.inflate(context, R.layout.badge_view, this)
    }

    fun setImageResource(@DrawableRes resourceId: Int) {
        iconIv.setImageResource(resourceId)
        iconIv.show()
        updateMarginBetweenTextAndImage()
    }

    fun setImageDrawable(drawable: Drawable?) {
        iconIv.setImageDrawable(drawable)
        iconIv.show()
        updateMarginBetweenTextAndImage()
    }

    fun setImageBitmap(bitmap: Bitmap) {
        iconIv.setImageBitmap(bitmap)
        iconIv.show()
        updateMarginBetweenTextAndImage()
    }

    fun setText(text: String?) {
        textTv.setTextOrGone(text)
        updateMarginBetweenTextAndImage()
    }

    fun setBackgroundColor(@ColorInt color: Int?) {
        this.setBackgroundTint(color)
    }

    fun setTintColor(@ColorInt color: Int) {
        textTv.setTextColor(color)
        iconIv.setColorFilter(color)
    }

    private fun updateMarginBetweenTextAndImage() {
        (iconIv.layoutParams as? LayoutParams)?.marginStart =
            if (textTv.isVisible && iconIv.isVisible) context.dip(4) else 0
    }

    private fun View.setBackgroundTint(@ColorInt color: Int?) {
        if (color != null)
            ViewCompat.setBackgroundTintList(this, ColorStateList.valueOf(color))
        else
            ViewCompat.setBackgroundTintList(this, null)
    }
}
