package com.semisonfire.compose.mvi.sample.adapter.text

import com.semisonfire.compose.mvi.sample.R
import com.semisonfire.compose.mvi.sample.adapter.Item

data class TextItem(
    val text: String
) : Item {

    override fun areItemsSame(item: Item): Boolean {
        if (item !is TextItem) return false

        return this.getItemType() == item.getItemType()
    }

    override fun areContentSame(item: Item): Boolean {
        if (item !is TextItem) return false

        return this == item
    }

    override fun getItemType(): Int {
        return R.layout.item_text
    }
}