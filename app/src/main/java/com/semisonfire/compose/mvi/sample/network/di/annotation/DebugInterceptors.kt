package com.semisonfire.compose.mvi.sample.network.di.annotation

import javax.inject.Qualifier

@Qualifier
@Retention(AnnotationRetention.RUNTIME)
annotation class DebugInterceptors