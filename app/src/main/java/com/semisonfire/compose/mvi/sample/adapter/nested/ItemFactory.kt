package com.semisonfire.compose.mvi.sample.adapter.nested

import android.graphics.Canvas
import android.graphics.Rect
import android.view.View
import android.view.ViewGroup
import androidx.core.view.children
import androidx.recyclerview.widget.RecyclerView
import com.semisonfire.compose.mvi.sample.R
import com.semisonfire.compose.mvi.sample.adapter.Item
import com.semisonfire.compose.mvi.sample.adapter.badge.BadgeItemViewHolder
import com.semisonfire.compose.mvi.sample.adapter.button.ButtonItemViewHolder
import com.semisonfire.compose.mvi.sample.adapter.image.ImageItemViewHolder
import com.semisonfire.compose.mvi.sample.adapter.text.TextItemViewHolder
import com.semisonfire.compose.mvi.sample.utils.dip

class ItemFactory {

    fun create(parent: ViewGroup, viewType: Int): ItemViewHolder<out Item>? {

        return when (viewType) {
            R.layout.item_text -> TextItemViewHolder(parent)
            R.layout.item_image -> ImageItemViewHolder(parent)
            R.layout.badge_view -> BadgeItemViewHolder(parent)
            R.layout.item_button -> ButtonItemViewHolder(parent)
            else -> null
        }
    }
}

class ItemDecorator : RecyclerView.ItemDecoration() {

    override fun getItemOffsets(
        outRect: Rect,
        view: View,
        parent: RecyclerView,
        state: RecyclerView.State
    ) {
        super.getItemOffsets(outRect, view, parent, state)

        val inner = parent.context.dip(4)
        val edge = inner * 2

        outRect.top = inner
        outRect.bottom = inner
        outRect.left = edge
        outRect.right = edge

        when (parent.getChildAdapterPosition(view)) {
            0 -> outRect.top = edge
            parent.adapter?.itemCount -> outRect.bottom = edge
        }
    }

    override fun onDraw(c: Canvas, parent: RecyclerView, state: RecyclerView.State) {
        super.onDraw(c, parent, state)

        parent.children.forEach {
            val holder = parent.getChildViewHolder(it)
            if (holder is ButtonItemViewHolder) {
                val parentBottom = parent.bottom
                val y = (parentBottom - it.height).toFloat()
                if (it.y != y) {
                    it.y = y
                }
                return
            }
        }
    }
}