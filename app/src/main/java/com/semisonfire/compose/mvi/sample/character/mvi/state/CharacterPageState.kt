package com.semisonfire.compose.mvi.sample.character.mvi.state

import com.semisonfire.compose.mvi.sample.adapter.Item

data class CharacterPageState(
    val current: String,
    val next: String?,
    val items: List<Item>,
    val loading: Boolean
)