package com.semisonfire.compose.mvi.sample.adapter

interface Item {

    fun getItemType(): Int = 0

    fun areItemsSame(item: Item): Boolean
    fun areContentSame(item: Item): Boolean
}