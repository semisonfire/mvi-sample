package com.semisonfire.compose.mvi.sample.adapter.button

import com.semisonfire.compose.mvi.sample.R
import com.semisonfire.compose.mvi.sample.adapter.Item

data class ButtonItem(
    val name: String,
    val text: String
) : Item {

    override fun getItemType(): Int {
        return R.layout.item_button
    }

    override fun areItemsSame(item: Item): Boolean {
        return getItemType() == item.getItemType()
    }

    override fun areContentSame(item: Item): Boolean {
        return this == item
    }
}