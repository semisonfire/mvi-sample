package com.semisonfire.compose.mvi.sample.adapter.image

import com.semisonfire.compose.mvi.sample.R
import com.semisonfire.compose.mvi.sample.adapter.Item

data class ImageItem(
    val image: String
) : Item {

    override fun areItemsSame(item: Item): Boolean {
        if (item !is ImageItem) return false

        return this.getItemType() == item.getItemType()
    }

    override fun areContentSame(item: Item): Boolean {
        if (item !is ImageItem) return false

        return this == item
    }

    override fun getItemType(): Int {
        return R.layout.item_image
    }
}