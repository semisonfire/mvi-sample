package com.semisonfire.compose.mvi.sample.tab.di

import androidx.fragment.app.FragmentManager
import com.semisonfire.compose.mvi.sample.di.ApplicationComponent
import com.semisonfire.compose.mvi.sample.main.di.MainComponentApi
import com.semisonfire.compose.mvi.sample.navigation.Navigator
import com.semisonfire.compose.mvi.sample.navigation.di.NavigationScope
import com.semisonfire.compose.mvi.sample.tab.TabFragment
import com.semisonfire.compose.mvi.sample.tab.di.annotation.TabNavigator
import com.semisonfire.compose.mvi.sample.tab.di.annotation.TabScope
import com.semisonfire.compose.mvi.sample.tab.di.module.TabModule
import dagger.BindsInstance
import dagger.Component

@TabScope
@NavigationScope
@Component(
    dependencies = [
        ApplicationComponent::class,
        MainComponentApi::class
    ],
    modules = [
        TabModule::class
    ]
)
interface TabComponent {

    @TabNavigator
    fun navigator(): Navigator

    fun inject(screen: TabFragment)

    @Component.Factory
    interface Factory {

        fun create(
            @BindsInstance fragmentManager: FragmentManager,
            applicationComponent: ApplicationComponent,
            mainComponent: MainComponentApi
        ): TabComponent
    }
}