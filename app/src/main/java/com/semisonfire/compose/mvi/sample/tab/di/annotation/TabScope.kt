package com.semisonfire.compose.mvi.sample.tab.di.annotation

import javax.inject.Scope

@Scope
@Retention
annotation class TabScope
