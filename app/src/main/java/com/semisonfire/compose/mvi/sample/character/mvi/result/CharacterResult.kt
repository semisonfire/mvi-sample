package com.semisonfire.compose.mvi.sample.character.mvi.result

import com.semisonfire.compose.mvi.sample.character.CharacterEffect
import com.semisonfire.compose.mvi.sample.character.mvi.command.CharacterCommand
import com.semisonfire.compose.mvi.sample.character.mvi.state.CharacterState

data class CharacterResult(
    val state: CharacterState,
    val command: CharacterCommand? = null,
    val effect: CharacterEffect? = null
)