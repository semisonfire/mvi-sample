package com.semisonfire.compose.mvi.sample.di

import android.app.Application
import com.google.gson.Gson
import com.semisonfire.compose.mvi.sample.network.api.RemoteApi
import com.semisonfire.compose.mvi.sample.network.di.NetworkModule
import dagger.BindsInstance
import dagger.Component
import okhttp3.OkHttpClient
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        NetworkModule::class
    ]
)
interface ApplicationComponent {

    fun api(): RemoteApi
    fun httpClient(): OkHttpClient
    fun gson(): Gson

    @Component.Factory
    interface Factory {

        fun create(
            @BindsInstance application: Application
        ): ApplicationComponent
    }
}