package com.semisonfire.compose.mvi.sample.character.api

data class CharacterResponse(
    val results: List<Character>
) {

    data class Character(
        val id: Int,
        val name: String,
        val status: String,
        val image: String,
    )
}