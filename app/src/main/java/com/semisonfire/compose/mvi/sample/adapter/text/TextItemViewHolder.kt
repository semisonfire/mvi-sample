package com.semisonfire.compose.mvi.sample.adapter.text

import android.view.LayoutInflater
import android.view.ViewGroup
import com.semisonfire.compose.mvi.sample.R
import com.semisonfire.compose.mvi.sample.adapter.nested.ItemViewHolder
import com.semisonfire.compose.mvi.sample.databinding.ItemTextBinding

class TextItemViewHolder(parent: ViewGroup) : ItemViewHolder<TextItem>(
    LayoutInflater.from(parent.context).inflate(R.layout.item_text, parent, false)
) {

    private val viewBinding = ItemTextBinding.bind(itemView)

    override fun bind(item: TextItem) {
        super.bind(item)
        viewBinding.root.text = item.text
    }
}