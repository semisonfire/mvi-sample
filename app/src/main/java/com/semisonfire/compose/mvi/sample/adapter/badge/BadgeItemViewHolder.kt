package com.semisonfire.compose.mvi.sample.adapter.badge

import android.view.ViewGroup
import com.semisonfire.compose.mvi.sample.adapter.nested.ItemViewHolder

class BadgeItemViewHolder(parent: ViewGroup) : ItemViewHolder<BadgeItem>(
    BadgeView(parent.context)
) {

    override fun bind(item: BadgeItem) {
        super.bind(item)
        (itemView as BadgeView).apply {
            setText(item.text)
            setBackgroundColor(item.color)
        }
    }
}