package com.semisonfire.compose.mvi.sample.di.provider

import android.content.Context
import androidx.fragment.app.Fragment

interface ComponentProvider<T> {

    fun component(): T?
}

inline fun <reified T> Context.provideComponent(): T {

    var component: T? = null
    if (this is ComponentProvider<*>) {
        component = this.component() as? T
    }

    if (component == null) {
        component = (applicationContext as? ComponentProvider<T>)?.component()
    }

    return component ?: throw IllegalArgumentException("Component not found")
}

inline fun <reified T> Fragment.provideComponent(): T {

    var parent = parentFragment
    while (parent != null) {
        if (parent is ComponentProvider<*>) {
            val component = parent.component() as? T
            if (component != null) {
                return component
            }
        }

        parent = parent.parentFragment
    }

    return requireContext().provideComponent()
}