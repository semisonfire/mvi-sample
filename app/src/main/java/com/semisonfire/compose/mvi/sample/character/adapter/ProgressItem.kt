package com.semisonfire.compose.mvi.sample.character.adapter

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.semisonfire.compose.mvi.sample.adapter.Item

class ProgressItem : Item {

    override fun areItemsSame(item: Item): Boolean {
        if (item !is ProgressItem) return false

        return this == item
    }

    override fun areContentSame(item: Item): Boolean {
        if (item !is ProgressItem) return false

        return this == item
    }
}

class ProgressViewHolder(view: View) : RecyclerView.ViewHolder(view)