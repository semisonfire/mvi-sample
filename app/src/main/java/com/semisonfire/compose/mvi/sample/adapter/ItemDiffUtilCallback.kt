package com.semisonfire.compose.mvi.sample.adapter

import androidx.recyclerview.widget.DiffUtil

class ItemDiffUtilCallback<I : Item> : DiffUtil.ItemCallback<I>() {
    override fun areItemsTheSame(oldItem: I, newItem: I): Boolean {
        return oldItem.areItemsSame(newItem)
    }

    override fun areContentsTheSame(oldItem: I, newItem: I): Boolean {
        return oldItem.areContentSame(newItem)
    }
}