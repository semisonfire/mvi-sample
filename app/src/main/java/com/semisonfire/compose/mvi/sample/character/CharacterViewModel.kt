package com.semisonfire.compose.mvi.sample.character

import androidx.lifecycle.ViewModel
import com.semisonfire.compose.mvi.sample.character.adapter.ProgressItem
import com.semisonfire.compose.mvi.sample.character.mvi.command.CharacterCommand
import com.semisonfire.compose.mvi.sample.character.mvi.event.CharacterEvent
import com.semisonfire.compose.mvi.sample.character.mvi.result.CharacterResult
import com.semisonfire.compose.mvi.sample.character.mvi.state.CharacterState
import com.semisonfire.compose.mvi.sample.logger.debugLog
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.schedulers.Schedulers
import io.reactivex.rxjava3.subjects.BehaviorSubject
import io.reactivex.rxjava3.subjects.PublishSubject
import javax.inject.Inject

sealed class CharacterEffect {
    class ScrollTo(val position: Int) : CharacterEffect()
}

class CharacterViewModel @Inject constructor(
    repository: CharacterRepository
) : ViewModel() {

    private val disposables = CompositeDisposable()

    private val actor = Actor(repository)
    private val reducer = Reducer()

    private val effectListener = PublishSubject.create<CharacterEffect>()
    private val eventListener = PublishSubject.create<CharacterEvent>()
    private val commandListener = PublishSubject.create<CharacterCommand>()

    private val stateListener = BehaviorSubject.createDefault(CharacterState.initialState())

    init {
        disposables.addAll(
            eventListener
                .observeOn(Schedulers.computation())
                .withLatestFrom(stateListener) { event, state ->

                    debugLog("new event $event")

                    val result = reducer.reduce(state, event)
                    stateListener.onNext(result.state)

                    // effects
                    val effect = result.effect
                    if (effect != null) {
                        debugLog("new effect $effect")

                        effectListener.onNext(effect)
                    }

                    result
                }
                .filter { it.command != null }
                .subscribe {
                    commandListener.onNext(it.command!!)
                },

            commandListener
                .flatMap {
                    debugLog("new command $it")
                    actor.execute(it)
                }
                .subscribe {
                    eventListener.onNext(it)
                }
        )
    }

    fun loadCharacters() {
        eventListener.onNext(CharacterEvent.Load())
    }

    fun loadMore() {
        eventListener.onNext(CharacterEvent.LoadMore())
    }

    fun observeState(): Observable<CharacterState> {
        return stateListener.distinctUntilChanged().hide()
    }

    fun observeEffects(): Observable<CharacterEffect> {
        return effectListener.hide()
    }

    private class Actor(
        private val repository: CharacterRepository
    ) {
        fun execute(command: CharacterCommand): Observable<CharacterEvent> {
            return when (command) {
                is CharacterCommand.Load -> {
                    repository
                        .fetchPage(command.page)
                        .map<CharacterEvent> { CharacterEvent.Loaded(it, false) }
                        .onErrorReturn { CharacterEvent.Failed(it) }
                }
                is CharacterCommand.LoadMore -> {
                    repository
                        .fetchPage(command.page)
                        .map<CharacterEvent> { CharacterEvent.Loaded(it, true) }
                        .onErrorReturn { CharacterEvent.Failed(it) }
                }
            }.toObservable()
        }
    }

    private class Reducer {

        fun reduce(
            state: CharacterState,
            event: CharacterEvent
        ): CharacterResult {
            return when (event) {
                is CharacterEvent.Load -> {
                    CharacterResult(
                        state = state.copy(
                            loader = true,
                            pageState = state.pageState.copy(
                                next = null,
                                items = emptyList(),
                                loading = true
                            ),
                            error = null
                        ),
                        command = CharacterCommand.Load(state.pageState.current)
                    )
                }
                is CharacterEvent.LoadMore -> {
                    if (state.pageState.loading) return CharacterResult(state, null)
                    val next = state.pageState.next ?: return CharacterResult(state, null)

                    val items = state.pageState.items
                        .toMutableList()
                        .apply { add(ProgressItem()) }
                    CharacterResult(
                        state = state.copy(
                            pageState = state.pageState.copy(
                                loading = true,
                                items = items
                            ),
                            loader = false,
                            error = null
                        ),
                        command = CharacterCommand.LoadMore(next)
                    )
                }
                is CharacterEvent.Loaded -> {
                    val items = if (event.loadMore) {
                        state.pageState.items
                            .toMutableList()
                            .apply { removeAll { it is ProgressItem } } + event.pageInfo.items
                    } else {
                        event.pageInfo.items
                    }

                    CharacterResult(
                        state = state.copy(
                            pageState = state.pageState.copy(
                                next = event.pageInfo.next,
                                items = items,
                                loading = false
                            ),
                            loader = false,
                            error = null
                        ),
                        command = null
                    )
                }
                is CharacterEvent.Failed -> {
                    CharacterResult(
                        state.copy(
                            pageState = state.pageState.copy(
                                loading = false
                            ),
                            loader = false,
                            error = event.throwable
                        ),
                        null
                    )
                }
            }
        }
    }

    override fun onCleared() {
        super.onCleared()
        disposables.dispose()
    }
}