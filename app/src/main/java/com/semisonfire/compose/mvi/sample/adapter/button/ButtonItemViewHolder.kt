package com.semisonfire.compose.mvi.sample.adapter.button

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Toast
import com.semisonfire.compose.mvi.sample.R
import com.semisonfire.compose.mvi.sample.adapter.nested.ItemViewHolder
import com.semisonfire.compose.mvi.sample.databinding.ItemButtonBinding

class ButtonItemViewHolder(parent: ViewGroup) : ItemViewHolder<ButtonItem>(
    LayoutInflater.from(parent.context).inflate(R.layout.item_button, parent, false)
) {
    private val viewBinding = ItemButtonBinding.bind(itemView)

    init {
        itemView.setOnClickListener {
            Toast.makeText(it.context, "Open ${item?.name}", Toast.LENGTH_SHORT).show()
        }
    }

    override fun bind(item: ButtonItem) {
        super.bind(item)
        viewBinding.root.text = item.text
    }
}