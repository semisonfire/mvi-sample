package com.semisonfire.compose.mvi.sample.character.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.semisonfire.compose.mvi.sample.R
import com.semisonfire.compose.mvi.sample.adapter.Item
import com.semisonfire.compose.mvi.sample.adapter.ItemDiffUtilCallback
import com.semisonfire.compose.mvi.sample.adapter.nested.ItemAdapter
import com.semisonfire.compose.mvi.sample.adapter.nested.ItemDecorator
import com.semisonfire.compose.mvi.sample.databinding.ItemCharacterBinding

class CharacterAdapter :
    ListAdapter<Item, RecyclerView.ViewHolder>(ItemDiffUtilCallback()) {

    private val viewPool = RecyclerView.RecycledViewPool()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(viewType, parent, false)
        return when (viewType) {
            R.layout.item_character -> CharacterViewHolder(view, viewPool)
            R.layout.item_progress -> ProgressViewHolder(view)
            else -> throw IllegalArgumentException("View type not supported $viewType")
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is CharacterViewHolder -> holder.bind(getItem(position) as CharacterItem)
        }
    }

    override fun getItemViewType(position: Int): Int {
        return when (getItem(position)) {
            is CharacterItem -> R.layout.item_character
            is ProgressItem -> R.layout.item_progress
            else -> super.getItemViewType(position)
        }
    }

    override fun onViewAttachedToWindow(holder: RecyclerView.ViewHolder) {
        super.onViewAttachedToWindow(holder)

        if (holder is CharacterViewHolder)
        holder.attach()
    }

    override fun onViewDetachedFromWindow(holder: RecyclerView.ViewHolder) {
        super.onViewDetachedFromWindow(holder)
        if (holder is CharacterViewHolder)
        holder.detach()
    }

    override fun onViewRecycled(holder: RecyclerView.ViewHolder) {
        super.onViewRecycled(holder)
        if (holder is CharacterViewHolder)
        holder.recycle()
    }

    class CharacterViewHolder(
        view: View,
        viewPool: RecyclerView.RecycledViewPool
    ) : RecyclerView.ViewHolder(view) {

        private val context = view.context
        private val viewBinding = ItemCharacterBinding.bind(view)

        private val adapter = ItemAdapter()

        init {
            viewBinding.characterState.adapter = adapter
            viewBinding.characterState.itemAnimator = null
            viewBinding.characterState.layoutManager =
                object : LinearLayoutManager(context, RecyclerView.VERTICAL, false) {
                    override fun canScrollHorizontally(): Boolean {
                        return false
                    }

                    override fun canScrollVertically(): Boolean {
                        return false
                    }
                }
//                .also { it.recycleChildrenOnDetach = true }
            viewBinding.characterState.setRecycledViewPool(viewPool)
            viewBinding.characterState.addItemDecoration(ItemDecorator())
        }

        fun bind(item: CharacterItem) {
            adapter.submitList(item.state)
        }

        fun attach() {}
        fun detach() {}
        fun recycle() {}
    }
}