package com.semisonfire.compose.mvi.sample.character

import android.content.Context
import android.os.Bundle
import android.view.View
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.semisonfire.compose.mvi.sample.R
import com.semisonfire.compose.mvi.sample.character.adapter.CharacterAdapter
import com.semisonfire.compose.mvi.sample.character.adapter.CharacterItem
import com.semisonfire.compose.mvi.sample.character.di.DaggerCharacterComponent
import com.semisonfire.compose.mvi.sample.character.mvi.state.CharacterState
import com.semisonfire.compose.mvi.sample.core.paging.PagingScrollListener
import com.semisonfire.compose.mvi.sample.databinding.FragmentCharacterBinding
import com.semisonfire.compose.mvi.sample.di.provider.provideComponent
import com.semisonfire.compose.mvi.sample.utils.viewModelFactory
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.disposables.Disposable
import javax.inject.Inject
import javax.inject.Provider

class CharacterFragment : Fragment(R.layout.fragment_character) {

    companion object {
        fun newInstance(): Fragment {
            return CharacterFragment()
        }
    }

    @Inject
    lateinit var pViewModel: Provider<CharacterViewModel>

    private val adapter = CharacterAdapter()
    private val viewModel by viewModels<CharacterViewModel> { viewModelFactory { pViewModel.get() } }

    private var stateDisposable: Disposable? = null
    private var effectDisposable: Disposable? = null

    private var _viewBinding: FragmentCharacterBinding? = null
    private val viewBinding
        get() = _viewBinding!!

    override fun onAttach(context: Context) {
        DaggerCharacterComponent
            .factory()
            .create(
                provideComponent(),
                provideComponent()
            )
            .inject(this)

        super.onAttach(context)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        _viewBinding = FragmentCharacterBinding.bind(view)

        viewBinding.characterList.adapter = adapter
        viewBinding.characterList.layoutManager = GridLayoutManager(view.context, 2, RecyclerView.VERTICAL, false)
        viewBinding.characterList.addOnScrollListener(
            PagingScrollListener {
                viewModel.loadMore()
            }
        )
        viewBinding.characterSwipeRefresh.setOnRefreshListener {
            viewModel.loadCharacters()
        }

        viewModel.loadCharacters()
    }

    override fun onStart() {
        super.onStart()
        stateDisposable =
            viewModel
                .observeState()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    render(it)
                }
    }

    private fun render(state: CharacterState) {
        viewBinding.characterSwipeRefresh.isRefreshing = false

        if (state.loader) {
            viewBinding.characterProgress.isVisible = true
            return
        }

        viewBinding.characterProgress.isVisible = false
        adapter.submitList(state.pageState.items as List<CharacterItem>)
    }


    override fun onResume() {
        super.onResume()
        effectDisposable = viewModel
            .observeEffects()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                applyEffect(it)
            }
    }

    private fun applyEffect(effect: CharacterEffect) {
        when (effect) {
            is CharacterEffect.ScrollTo -> viewBinding.characterList.scrollToPosition(effect.position)
        }
    }

    override fun onPause() {
        super.onPause()
        effectDisposable?.dispose()
        effectDisposable = null
    }

    override fun onStop() {
        super.onStop()
        stateDisposable?.dispose()
        stateDisposable = null
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _viewBinding = null
    }
}