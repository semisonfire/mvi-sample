package com.semisonfire.compose.mvi.sample.character

import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.semisonfire.compose.mvi.sample.character.api.CharacterResponse
import com.semisonfire.compose.mvi.sample.character.di.CharacterScope
import com.semisonfire.compose.mvi.sample.core.data.page.PageInfo
import com.semisonfire.compose.mvi.sample.core.data.page.PageParser
import com.semisonfire.compose.mvi.sample.network.api.RemoteApi
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.schedulers.Schedulers
import java.lang.reflect.Type
import javax.inject.Inject

@CharacterScope
class CharacterRepository @Inject constructor(
    private val api: RemoteApi,
    private val parser: PageParser,
    private val gson: Gson,
    private val mapper: CharacterMapper
) {
    private val listType: Type = object : TypeToken<List<CharacterResponse.Character>>() {}.type

    fun fetchPage(page: String): Single<PageInfo> {
        return api
            .fetchPage(page)
            .observeOn(Schedulers.computation())
            .map { response ->
                val json = response.body()?.string().orEmpty()
                val pageInfo = parser.parse(page, json)
                val characters = mapper.map(
                    parseResponse(pageInfo.data)
                )

                PageInfo(
                    current = pageInfo.current,
                    next = pageInfo.next,
                    items = characters
                )
            }
    }

    private fun parseResponse(data: String): List<CharacterResponse.Character> {
        if (data.isEmpty()) return emptyList()

        return gson.fromJson(data, listType) ?: emptyList()
    }
}
