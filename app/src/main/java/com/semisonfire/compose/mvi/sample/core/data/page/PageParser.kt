package com.semisonfire.compose.mvi.sample.core.data.page

import org.json.JSONObject
import javax.inject.Inject

class PageParser @Inject constructor() {

    companion object {
        private const val FIELD_INFO = "info"
        private const val FIELD_NEXT_PAGE = "next"

        private const val FIELD_DATA = "results"
        private const val FIELD_ERROR = "error"
    }

    @Throws(IllegalStateException::class)
    fun parse(page: String, json: String): PageRawInfo {
        val jsonObject = JSONObject(json)

        val error = parseError(jsonObject)
        if (error != null) {
            throw IllegalStateException(error)
        }

        val data = jsonObject.optJSONArray(FIELD_DATA)?.toString() ?: json
        val nextPage = parseNextPage(jsonObject)

        return PageRawInfo(
            current = page,
            next = nextPage,
            data = data
        )
    }

    private fun parseNextPage(jsonObject: JSONObject): String? {
        return jsonObject.optJSONObject(FIELD_INFO)?.optString(FIELD_NEXT_PAGE)
    }

    private fun parseError(jsonObject: JSONObject): String? {
        return jsonObject.optString(FIELD_ERROR).takeIf { it.isNotBlank() }
    }
}