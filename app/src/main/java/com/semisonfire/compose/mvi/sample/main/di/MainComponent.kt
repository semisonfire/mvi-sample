package com.semisonfire.compose.mvi.sample.main.di

import androidx.fragment.app.FragmentManager
import com.semisonfire.compose.mvi.sample.di.ApplicationComponent
import com.semisonfire.compose.mvi.sample.main.MainActivity
import com.semisonfire.compose.mvi.sample.main.di.annotation.MainScope
import com.semisonfire.compose.mvi.sample.main.di.module.MainModule
import com.semisonfire.compose.mvi.sample.navigation.Navigator
import com.semisonfire.compose.mvi.sample.navigation.di.NavigationScope
import dagger.BindsInstance
import dagger.Component

@MainScope
@NavigationScope
@Component(
    modules = [
        MainModule::class
    ],
    dependencies = [
        ApplicationComponent::class
    ]
)
interface MainComponent : MainComponentApi {

    fun inject(screen: MainActivity)

    @Component.Factory
    interface Factory {
        fun create(
            @BindsInstance fragmentManager: FragmentManager,
            applicationComponent: ApplicationComponent
        ): MainComponent
    }
}

interface MainComponentApi {
    fun navigator(): Navigator
}