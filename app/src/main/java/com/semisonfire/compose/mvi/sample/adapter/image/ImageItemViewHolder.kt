package com.semisonfire.compose.mvi.sample.adapter.image

import android.view.LayoutInflater
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.semisonfire.compose.mvi.sample.R
import com.semisonfire.compose.mvi.sample.adapter.nested.ItemViewHolder
import com.semisonfire.compose.mvi.sample.databinding.ItemImageBinding

class ImageItemViewHolder(parent: ViewGroup) : ItemViewHolder<ImageItem>(
    LayoutInflater.from(parent.context).inflate(R.layout.item_image, parent, false)
) {

    private val viewBinding = ItemImageBinding.bind(itemView)

    override fun bind(item: ImageItem) {
        super.bind(item)
        Glide
            .with(itemView.context)
            .load(item.image)
            .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.ALL))
            .into(viewBinding.root)
    }
}