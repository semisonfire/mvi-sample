package com.semisonfire.compose.mvi.sample.character.mvi.command

sealed class CharacterCommand {
    class Load(val page: String) : CharacterCommand()
    class LoadMore(val page: String) : CharacterCommand()
}