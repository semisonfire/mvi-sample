package com.semisonfire.compose.mvi.sample.tab.di.annotation

import javax.inject.Qualifier

@Qualifier
@Retention
annotation class TabNavigator