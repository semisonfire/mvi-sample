package com.semisonfire.compose.mvi.sample.character.mvi.event

import com.semisonfire.compose.mvi.sample.core.data.page.PageInfo

sealed class CharacterEvent {

    class Load : CharacterEvent()
    class LoadMore : CharacterEvent()

    class Loaded(
        val pageInfo: PageInfo,
        val loadMore: Boolean
    ) : CharacterEvent()

    class Failed(val throwable: Throwable) : CharacterEvent()
}