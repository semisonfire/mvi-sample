package com.semisonfire.compose.mvi.sample.network.api

import io.reactivex.rxjava3.core.Single
import okhttp3.ResponseBody
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Url

interface RemoteApi {

    @GET
    fun fetchPage(@Url page: String): Single<Response<ResponseBody>>
}