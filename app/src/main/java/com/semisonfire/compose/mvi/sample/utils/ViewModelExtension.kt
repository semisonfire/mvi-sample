package com.semisonfire.compose.mvi.sample.utils

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

inline fun <T : ViewModel> viewModelFactory(crossinline provider: () -> T): ViewModelProvider.Factory {

    return object : ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return provider.invoke() as T
        }
    }
}