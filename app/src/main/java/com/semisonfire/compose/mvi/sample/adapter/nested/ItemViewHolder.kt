package com.semisonfire.compose.mvi.sample.adapter.nested

import android.view.View
import androidx.annotation.CallSuper
import androidx.recyclerview.widget.RecyclerView
import com.semisonfire.compose.mvi.sample.adapter.Item

abstract class ItemViewHolder<I : Item>(view: View) : RecyclerView.ViewHolder(view) {

    protected var item: I? = null

    @CallSuper
    open fun bind(item: I) {
        this.item = item
    }

    open fun attach() {}
    open fun detach() {}
    open fun recycle() {}
}