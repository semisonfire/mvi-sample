package com.semisonfire.compose.mvi.sample.character.adapter

import com.semisonfire.compose.mvi.sample.adapter.Item

data class CharacterItem(
    val id: Int,
    val state: List<Item>
) : Item {

    override fun areItemsSame(item: Item): Boolean {
        if (item !is CharacterItem) return false

        return id == item.id
    }

    override fun areContentSame(item: Item): Boolean {
        if (item !is CharacterItem) return false

        return this == item
    }
}