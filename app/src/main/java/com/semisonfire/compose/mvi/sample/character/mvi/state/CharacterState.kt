package com.semisonfire.compose.mvi.sample.character.mvi.state

import com.semisonfire.compose.mvi.sample.network.di.NetworkModule

data class CharacterState(
    val pageState: CharacterPageState,
    val loader: Boolean,
    val error: Throwable?
) {

    companion object {

        private const val CHARACTERS = "character"

        fun initialState(): CharacterState {
            return CharacterState(
                pageState = CharacterPageState(
                    current = NetworkModule.apiUrl
                        .buildUpon()
                        .appendPath(CHARACTERS)
                        .build()
                        .toString(),
                    next = null,
                    items = emptyList(),
                    loading = false,
                ),
                loader = false,
                error = null
            )
        }
    }
}