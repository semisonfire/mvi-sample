package com.semisonfire.compose.mvi.sample.character

import android.graphics.Color
import com.semisonfire.compose.mvi.sample.adapter.Item
import com.semisonfire.compose.mvi.sample.adapter.badge.BadgeItem
import com.semisonfire.compose.mvi.sample.adapter.button.ButtonItem
import com.semisonfire.compose.mvi.sample.adapter.image.ImageItem
import com.semisonfire.compose.mvi.sample.adapter.text.TextItem
import com.semisonfire.compose.mvi.sample.character.adapter.CharacterItem
import com.semisonfire.compose.mvi.sample.character.api.CharacterResponse
import com.semisonfire.compose.mvi.sample.character.di.CharacterScope
import javax.inject.Inject
import kotlin.random.Random

@CharacterScope
class CharacterMapper @Inject constructor() {

    private val colors = arrayOf(
        Color.BLACK,
        Color.GREEN,
        Color.RED
    )

    fun map(characters: List<CharacterResponse.Character>): List<CharacterItem> {
        return characters.mapIndexed { index, it ->
            CharacterItem(
                it.id,
                generateState(index, it)
            )
        }
    }

    private fun generateState(index: Int, character: CharacterResponse.Character): List<Item> {

        val state = mutableListOf<Item>()
        state.add(ImageItem(character.image))

        state.add(TextItem(character.name))
        state.add(TextItem(character.status))

        if (index % 2 == 0) {
            state.add(stateIndex(state.size), BadgeItem(character.name, color()))
        } else {
            state.add(1, BadgeItem(character.name, color()))
            state.add(stateIndex(state.size), BadgeItem(character.status, color()))
        }

        state.add(ButtonItem(character.name, "Open"))

        return state
    }

    private fun stateIndex(until: Int): Int {
        return Random.nextInt(1, until)
    }

    private fun color(): Int {
        return colors[Random.nextInt(colors.size)]
    }
}