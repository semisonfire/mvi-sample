package com.semisonfire.compose.mvi.sample.utils

import android.content.Context
import android.view.View
import android.widget.TextView

fun Context.dipF(value: Int) = (value * resources.displayMetrics.density)
fun Context.dip(value: Int) = dipF(value).toInt()

fun View.show() {
    if (visibility != View.VISIBLE) {
        visibility = View.VISIBLE
    }
}

fun View.hide() {
    if (visibility != View.GONE) {
        visibility = View.GONE
    }
}

fun TextView.setTextOrGone(value: CharSequence?) {
    value
        ?.takeIf { it.trim().isNotEmpty() }
        ?.let { this.text = it; show() }
        ?: hide()
}