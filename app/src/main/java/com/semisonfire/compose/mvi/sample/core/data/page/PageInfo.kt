package com.semisonfire.compose.mvi.sample.core.data.page

import com.semisonfire.compose.mvi.sample.adapter.Item

data class PageInfo(
    val current: String,
    val next: String?,
    val items: List<Item>
)