package com.semisonfire.compose.mvi.sample.tab.di.module

import androidx.fragment.app.FragmentManager
import com.semisonfire.compose.mvi.sample.navigation.Navigator
import com.semisonfire.compose.mvi.sample.navigation.di.NavigationScope
import com.semisonfire.compose.mvi.sample.tab.di.annotation.TabNavigator
import dagger.Module
import dagger.Provides

@Module
class TabModule {

    @Provides
    @NavigationScope
    @TabNavigator
    fun provideTabNavigator(fragmentManager: FragmentManager): Navigator {
        return Navigator(fragmentManager)
    }
}