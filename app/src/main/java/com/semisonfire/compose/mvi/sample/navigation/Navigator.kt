package com.semisonfire.compose.mvi.sample.navigation

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.semisonfire.compose.mvi.sample.navigation.di.NavigationScope
import javax.inject.Inject

@NavigationScope
class Navigator @Inject constructor(
    private val fragmentManager: FragmentManager
) {

    fun open(id: Int, fragment: Fragment) {
        fragmentManager.beginTransaction()
            .apply {

                val tag = fragment::class.java.simpleName
                add(id, fragment, tag)
                setPrimaryNavigationFragment(fragment)
            }
            .commitNow()
    }

    fun replace(id: Int, fragment: Fragment) {

        val tag = fragment::class.java.simpleName
        val fragmentByTag = fragmentManager.findFragmentByTag(tag)

        fragmentManager.beginTransaction()
            .apply {
                if (fragmentByTag == null) {
                    replace(id, fragment, tag)
                    setPrimaryNavigationFragment(fragment)
                } else {
                    replace(id, fragmentByTag, tag)
                }
            }
            .commitNow()
    }
}