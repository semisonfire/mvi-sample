package com.semisonfire.compose.mvi.sample.network.di

import com.semisonfire.compose.mvi.sample.network.NetworkInterceptor
import com.semisonfire.compose.mvi.sample.network.di.annotation.DebugInterceptors
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoSet
import javax.inject.Singleton
import okhttp3.Interceptor
import okhttp3.logging.HttpLoggingInterceptor

@Module
internal abstract class NetworkInterceptorsModule {

    companion object {

        @Provides
        @IntoSet
        @DebugInterceptors
        @Singleton
        fun provideLoggingInterceptor(): Interceptor {
            val loggingInterceptor = HttpLoggingInterceptor()
            loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
            return loggingInterceptor
        }
    }

    @Binds
    @IntoSet
    abstract fun bindsNetworkInterceptor(impl: NetworkInterceptor): Interceptor
}