package com.semisonfire.compose.mvi.sample.network.di

import android.app.Application
import android.content.Context
import android.net.ConnectivityManager
import android.net.Uri
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.semisonfire.compose.mvi.sample.BuildConfig
import com.semisonfire.compose.mvi.sample.network.api.RemoteApi
import com.semisonfire.compose.mvi.sample.network.di.annotation.DebugInterceptors
import dagger.Module
import dagger.Provides
import io.reactivex.rxjava3.schedulers.Schedulers
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava3.RxJava3CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module(
    includes = [NetworkInterceptorsModule::class]
)
class NetworkModule {

    companion object {

        const val API_URL = "https://rickandmortyapi.com/api/"
        val apiUrl = Uri.parse(API_URL)
    }

    @Provides
    @Singleton
    fun provideGson(): Gson {
        return GsonBuilder().create()
    }

    @Provides
    @Singleton
    fun provideRetrofit(httpClient: OkHttpClient, gson: Gson): Retrofit {
        return Retrofit.Builder()
            .baseUrl(API_URL)
            .addCallAdapterFactory(RxJava3CallAdapterFactory.createWithScheduler(Schedulers.io()))
            .addConverterFactory(GsonConverterFactory.create(gson))
            .client(httpClient)
            .build()
    }

    @Provides
    @Singleton
    fun provideApi(retrofit: Retrofit): RemoteApi {
        return retrofit.create(RemoteApi::class.java)
    }

    @Provides
    @Singleton
    fun provideHttpClient(
        interceptors: Set<@JvmSuppressWildcards Interceptor>,
        @DebugInterceptors debugInterceptors: Set<@JvmSuppressWildcards Interceptor>,
    ): OkHttpClient {
        return OkHttpClient.Builder()
            .connectTimeout(30, TimeUnit.SECONDS)
            .readTimeout(30, TimeUnit.SECONDS)
            .writeTimeout(30, TimeUnit.SECONDS)
            .apply {
                interceptors.forEach { addInterceptor(it) }

                if (BuildConfig.DEBUG) {
                    debugInterceptors.forEach { addInterceptor(it) }
                }
            }
            .build()
    }

    @Provides
    @Singleton
    fun provideConnectivityManager(application: Application): ConnectivityManager {
        return application.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
    }
}